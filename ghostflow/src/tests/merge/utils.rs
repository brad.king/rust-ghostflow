// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::collections::HashMap;
use std::path::Path;
use std::process::Command;
use std::sync::Arc;

use chrono::{DateTime, Utc};
use git_workarea::{CommitId, GitContext, Identity};
use itertools::Itertools;
use lazy_static::lazy_static;

use crate::actions::merge::*;
use crate::host::{HostedProject, HostingService, User};
use crate::tests::mock::MockService;
use crate::tests::utils;
use crate::utils::Trailer;

const BRANCH_NAME: &str = "test-merge";
const BACKPORT_BRANCH_NAME: &str = "test-merge-backport";
const BACKPORT_BRANCH_NAME2: &str = "test-merge-backport2";
const INTO_BRANCH_NAME: &str = "test-merge-into";
const INTO_BRANCH_NAME2: &str = "test-merge-into2";
const BASE: &str = "58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd";

lazy_static! {
    static ref MERGER_IDENT: Identity = Identity::new("merger", "merger@example.com");
    static ref AUTHOR_DATE: DateTime<Utc> = Utc::now();
}

pub fn merger_ident() -> &'static Identity {
    &*MERGER_IDENT
}

pub fn author_date() -> DateTime<Utc> {
    *AUTHOR_DATE
}

pub fn git_context(workspace_path: &Path, commit: &str) -> (GitContext, GitContext) {
    // Here, we create two clones of the current repository: one to act as the remote and another
    // to be the repository the merge action uses. The first is cloned from the source tree's
    // directory while the second is cloned from that first clone. This sets up the `origin` remote
    // properly for the `merge` command.

    let origindir = workspace_path.join("origin");
    let clone = Command::new("git")
        .arg("clone")
        .arg("--bare")
        .arg("--single-branch")
        // Clone as a "remote" URL to only get the relevant objects. If a local URL is used, packs
        // may be shared which affects the `--auto-abbrev` used in the merge logic.
        .arg(concat!("file://", env!("CARGO_MANIFEST_DIR"), "/../.git"))
        .arg(&origindir)
        .output()
        .unwrap();
    if !clone.status.success() {
        panic!(
            "origin clone failed: {}",
            String::from_utf8_lossy(&clone.stderr),
        );
    }

    // Make the branches point to the expected place.
    let origin_ctx = GitContext::new(&origindir);
    let branches = &[
        BRANCH_NAME,
        BACKPORT_BRANCH_NAME,
        BACKPORT_BRANCH_NAME2,
        INTO_BRANCH_NAME,
        INTO_BRANCH_NAME2,
    ];
    for branch in branches {
        let update_ref = origin_ctx
            .git()
            .arg("update-ref")
            .arg(format!("refs/heads/{}", branch))
            .arg(commit)
            .output()
            .unwrap();
        if !update_ref.status.success() {
            panic!(
                "update-ref failed: {}",
                String::from_utf8_lossy(&update_ref.stderr),
            );
        }
    }

    let gitdir = workspace_path.join("git");
    let clone = Command::new("git")
        .arg("clone")
        .arg("--bare")
        .arg("--dissociate")
        .arg(origindir)
        .arg(&gitdir)
        .output()
        .unwrap();
    if !clone.status.success() {
        panic!(
            "working clone failed: {}",
            String::from_utf8_lossy(&clone.stderr)
        );
    }

    (origin_ctx, GitContext::new(gitdir))
}

pub fn make_commit(ctx: &GitContext, branch: &str) {
    let commit = ctx
        .git()
        .arg("commit-tree")
        .args(&["-p", branch])
        .args(&["-m", "blocking commit"])
        .arg(format!("{}^{{tree}}", branch))
        .output()
        .unwrap();
    if !commit.status.success() {
        panic!(
            "blocking commit creation failed: {}",
            String::from_utf8_lossy(&commit.stderr),
        );
    }
    let commit = String::from_utf8_lossy(&commit.stdout);
    let update_ref = ctx
        .git()
        .arg("update-ref")
        .arg(format!("refs/heads/{}", branch))
        .arg(commit.trim())
        .status()
        .unwrap();
    assert!(update_ref.success());
}

pub fn create_merge(
    git: &GitContext,
    base: &str,
    service: &Arc<MockService>,
) -> Merge<TestMergePolicy> {
    let settings = MergeSettings::new(base, TestMergePolicy::default());
    create_merge_settings(git, service, settings)
}

pub fn create_merge_settings(
    git: &GitContext,
    service: &Arc<MockService>,
    settings: MergeSettings<TestMergePolicy>,
) -> Merge<TestMergePolicy> {
    let project = HostedProject {
        name: "base".into(),
        service: Arc::clone(service) as Arc<dyn HostingService>,
    };
    Merge::new(git.clone(), project, settings)
}

pub fn create_merge_many(git: &GitContext, service: &Arc<MockService>) -> MergeMany {
    let project = HostedProject {
        name: "base".into(),
        service: Arc::clone(service) as Arc<dyn HostingService>,
    };
    MergeMany::new(git.clone(), project)
}

pub fn check_mr_commit_message(ctx: &GitContext, branch: &str, expected: &str) {
    let cat_file = ctx
        .git()
        .arg("cat-file")
        .arg("-p")
        .arg(format!("refs/heads/{}", branch))
        .output()
        .unwrap();
    assert!(cat_file.status.success());
    let actual = String::from_utf8_lossy(&cat_file.stdout)
        .splitn(2, "\n\n")
        .skip(1)
        .join("\n\n");
    assert_eq!(actual, expected);
}

pub fn check_mr_commit(ctx: &GitContext, branch: &str, expected: &str) {
    check_mr_commit_message(ctx, branch, expected);

    let author_log = ctx
        .git()
        .arg("log")
        .arg("--pretty=%an%n%ae%n%ad")
        .arg("--max-count=1")
        .arg(format!("refs/heads/{}", branch))
        .output()
        .unwrap();
    assert!(author_log.status.success());
    let actual = String::from_utf8_lossy(&author_log.stdout);
    let author_log_lines = actual.lines().collect::<Vec<_>>();
    assert_eq!(author_log_lines[0], merger_ident().name);
    assert_eq!(author_log_lines[1], merger_ident().email);
    assert_eq!(
        author_log_lines[2],
        author_date().format("%a %b %-d %H:%M:%S %Y %z").to_string(),
    );
}

pub fn check_mr_commit_ff(ctx: &GitContext, branch: &str, expected: &CommitId) {
    let commit = ctx
        .git()
        .arg("rev-parse")
        .arg(format!("refs/heads/{}", branch))
        .output()
        .unwrap();
    assert!(commit.status.success());
    let actual = String::from_utf8_lossy(&commit.stdout);
    assert_eq!(actual.trim(), expected.as_str());
}

#[derive(Debug)]
pub struct TestMergePolicy {
    trailers: Vec<Trailer>,
    needs_reviews_from: HashMap<String, String>,
    rejections: Vec<String>,
}

impl Default for TestMergePolicy {
    fn default() -> Self {
        TestMergePolicy {
            trailers: vec![Trailer::new(
                "Acked-by",
                "Ghostflow <ghostflow@example.com>",
            )],
            needs_reviews_from: HashMap::new(),
            rejections: Vec::new(),
        }
    }
}

impl MergePolicyFilter for TestMergePolicy {
    fn process_trailer(&mut self, trailer: &Trailer, user: Option<&User>) {
        // Skip trailers which do not end in `-by`.
        if !trailer.token.ends_with("-by") {
            return;
        }

        // Ignore trailers by the `ignore` user.
        if let Some(user) = user {
            if user.handle == "ignore" {
                return;
            }
        }

        if trailer.token == "Require-review-by" {
            if let Some(user) = user {
                let msg = format!("review is required by @{}", user.handle);
                self.needs_reviews_from.insert(user.handle.clone(), msg);
            }
        } else if trailer.token == "Rejected-by" {
            // Block if anyone else says `Rejected-by`.
            let reason = if let Some(user) = user {
                format!("rejected by @{}", user.handle)
            } else {
                format!("rejected by {}", trailer.value)
            };

            self.rejections.push(reason);
        } else {
            // Add the trailer.
            self.trailers.push(trailer.clone());
        }
    }

    fn result(self) -> ::std::result::Result<Vec<Trailer>, Vec<String>> {
        let reasons = self
            .rejections
            .into_iter()
            .chain(self.needs_reviews_from.into_iter().map(|(_, value)| value))
            .collect::<Vec<_>>();

        if reasons.is_empty() {
            Ok(self.trailers)
        } else {
            Err(reasons)
        }
    }
}

pub fn check_noop_merge(ctx: &GitContext, branch: &str) {
    let diff = ctx
        .git()
        .arg("diff")
        .arg("--exit-code")
        .arg(format!("refs/heads/{}", branch))
        .arg(format!("refs/heads/{}~", branch))
        .output()
        .unwrap();
    if !diff.status.success() {
        panic!(
            "expected no diff, found:\n{}",
            String::from_utf8_lossy(&diff.stdout),
        )
    }
}

pub fn simple_merge_behavior(service: Arc<MockService>, id: u64, message: &str) {
    let tempdir = utils::test_workspace_dir();
    let (origin_ctx, ctx) = git_context(tempdir.path(), BASE);
    let merge = create_merge(&ctx, BRANCH_NAME, &service);

    let mr = service.merge_request("base", id).unwrap();
    let res = merge.merge_mr(&mr, &MERGER_IDENT, *AUTHOR_DATE).unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    check_mr_commit(&origin_ctx, BRANCH_NAME, message);
}

pub fn failed_merge_behavior(service: Arc<MockService>, id: u64, message: &str) {
    let tempdir = utils::test_workspace_dir();
    let (origin_ctx, ctx) = git_context(tempdir.path(), BASE);
    let merge = create_merge(&ctx, BRANCH_NAME, &service);

    let mr = service.merge_request("base", id).unwrap();
    let res = merge.merge_mr(&mr, &MERGER_IDENT, *AUTHOR_DATE).unwrap();
    assert_eq!(res, MergeActionResult::Failed);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], message);

    check_mr_commit_message(&origin_ctx, BRANCH_NAME, "base: add a base commit\n");
}
