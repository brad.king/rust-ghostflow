// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::sync::Arc;

use crate::tests::merge::utils;
use crate::tests::mock::{MockData, MockMergeRequest, MockService};

const REPO_NAME: &str = "base";
const MR_PLUS_1_ID: u64 = 9;
const MR_PLUS_2_ID: u64 = 10;
const MR_PLUS_3_ID: u64 = 11;
const MR_BY_ME_ID: u64 = 12;
const MR_BY_OTHER_ID: u64 = 13;
const MR_BY_UNKNOWN_ID: u64 = 14;
const MR_BY_EXPLICIT_ID: u64 = 15;
const MR_OTHER_BY_TRAILER_ID: u64 = 16;
const MR_IGNORED_TRAILER_ID: u64 = 17;
const MR_AWARDED_100_ID: u64 = 18;
const MR_AWARDED_CLAP_ID: u64 = 19;
const MR_AWARDED_TADA_ID: u64 = 20;
const MR_AWARDED_THUMBSUP_ID: u64 = 21;
const MR_AWARDED_NOGOOD_ID: u64 = 22;
const MR_AWARDED_THUMBSDOWN_ID: u64 = 23;
const MR_AWARDED_THUMBSDOWN_TONE_ID: u64 = 24;
const MR_AWARDED_IGNORED_ID: u64 = 25;
const MR_DUPLICATE_TRAILER_ID: u64 = 26;
const COMMIT: &str = "7189cf557ba2c7c61881ff8669158710b94d8df1";

fn merge_service() -> Arc<MockService> {
    let base = MockData::repo(REPO_NAME);
    let fork = MockData::forked_repo("fork", &base);
    let user = MockData::user("user");
    let maint = MockData::user("maint");

    let mr = |id| MockMergeRequest::new(id, &user, COMMIT, &base, &fork);

    MockData::builder()
        .add_merge_request(mr(MR_PLUS_1_ID).add_comment(MockData::comment(&maint, "+1")))
        .add_merge_request(mr(MR_PLUS_2_ID).add_comment(MockData::comment(&maint, "+2")))
        .add_merge_request(mr(MR_PLUS_3_ID).add_comment(MockData::comment(&maint, "+3")))
        .add_merge_request(mr(MR_BY_ME_ID).add_comment(MockData::comment(&maint, "Acked-by: me")))
        .add_merge_request(
            mr(MR_BY_OTHER_ID).add_comment(MockData::comment(&maint, "Acked-by: @user")),
        )
        .add_merge_request(
            mr(MR_BY_UNKNOWN_ID).add_comment(MockData::comment(&maint, "Acked-by: @unknown")),
        )
        .add_merge_request(mr(MR_BY_EXPLICIT_ID).add_comment(MockData::comment(
            &maint,
            "Acked-by: User Name <email@example.invalid>",
        )))
        .add_merge_request(
            mr(MR_OTHER_BY_TRAILER_ID).add_comment(MockData::comment(&maint, "Reported-by: @user")),
        )
        .add_merge_request(
            mr(MR_IGNORED_TRAILER_ID).add_comment(MockData::comment(&maint, "Meaningless: traler")),
        )
        .add_merge_request(mr(MR_AWARDED_100_ID).add_award(MockData::award(&maint, "100")))
        .add_merge_request(mr(MR_AWARDED_CLAP_ID).add_award(MockData::award(&maint, "clap")))
        .add_merge_request(mr(MR_AWARDED_TADA_ID).add_award(MockData::award(&maint, "tada")))
        .add_merge_request(
            mr(MR_AWARDED_THUMBSUP_ID).add_award(MockData::award(&maint, "thumbsup")),
        )
        .add_merge_request(mr(MR_AWARDED_NOGOOD_ID).add_award(MockData::award(&maint, "no_good")))
        .add_merge_request(
            mr(MR_AWARDED_THUMBSDOWN_ID).add_award(MockData::award(&maint, "thumbsdown")),
        )
        .add_merge_request(
            mr(MR_AWARDED_THUMBSDOWN_TONE_ID)
                .add_award(MockData::award(&maint, "thumbsdown_tone1")),
        )
        .add_merge_request(mr(MR_AWARDED_IGNORED_ID).add_award(MockData::award(&maint, "balloon")))
        .add_merge_request(
            mr(MR_DUPLICATE_TRAILER_ID)
                .add_award(MockData::award(&maint, "tada"))
                .add_comment(MockData::comment(&maint, "Acked-by: me")),
        )
        .add_project(base)
        .add_project(fork)
        .add_user(maint)
        .add_user(user)
        .service()
}

#[test]
fn test_merge_plus_1() {
    let service = merge_service();
    let message = "Merge topic 'topic-9' into test-merge\n\
                   \n\
                   7189cf5 topic-1: make a change\n\
                   \n\
                   Acked-by: Ghostflow <ghostflow@example.com>\n\
                   Acked-by: maint <maint@example.invalid>\n\
                   Merge-request: !9\n";
    utils::simple_merge_behavior(service, MR_PLUS_1_ID, message)
}

#[test]
fn test_merge_plus_2() {
    let service = merge_service();
    let message = "Merge topic 'topic-10' into test-merge\n\
                   \n\
                   7189cf5 topic-1: make a change\n\
                   \n\
                   Acked-by: Ghostflow <ghostflow@example.com>\n\
                   Reviewed-by: maint <maint@example.invalid>\n\
                   Merge-request: !10\n";
    utils::simple_merge_behavior(service, MR_PLUS_2_ID, message)
}

#[test]
fn test_merge_plus_3() {
    let service = merge_service();
    let message = "Merge topic 'topic-11' into test-merge\n\
                   \n\
                   7189cf5 topic-1: make a change\n\
                   \n\
                   Acked-by: Ghostflow <ghostflow@example.com>\n\
                   Tested-by: maint <maint@example.invalid>\n\
                   Merge-request: !11\n";
    utils::simple_merge_behavior(service, MR_PLUS_3_ID, message)
}

#[test]
fn test_merge_trailer_by_me() {
    let service = merge_service();
    let message = "Merge topic 'topic-12' into test-merge\n\
                   \n\
                   7189cf5 topic-1: make a change\n\
                   \n\
                   Acked-by: Ghostflow <ghostflow@example.com>\n\
                   Acked-by: maint <maint@example.invalid>\n\
                   Merge-request: !12\n";
    utils::simple_merge_behavior(service, MR_BY_ME_ID, message)
}

#[test]
fn test_merge_trailer_by_other() {
    let service = merge_service();
    let message = "Merge topic 'topic-13' into test-merge\n\
                   \n\
                   7189cf5 topic-1: make a change\n\
                   \n\
                   Acked-by: Ghostflow <ghostflow@example.com>\n\
                   Acked-by: user <user@example.invalid>\n\
                   Merge-request: !13\n";
    utils::simple_merge_behavior(service, MR_BY_OTHER_ID, message)
}

#[test]
fn test_merge_trailer_by_unknown() {
    let service = merge_service();
    let message = "Merge topic 'topic-14' into test-merge\n\
                   \n\
                   7189cf5 topic-1: make a change\n\
                   \n\
                   Acked-by: Ghostflow <ghostflow@example.com>\n\
                   Merge-request: !14\n";
    utils::simple_merge_behavior(service, MR_BY_UNKNOWN_ID, message)
}

#[test]
fn test_merge_trailer_by_explicit() {
    let service = merge_service();
    let message = "Merge topic 'topic-15' into test-merge\n\
                   \n\
                   7189cf5 topic-1: make a change\n\
                   \n\
                   Acked-by: Ghostflow <ghostflow@example.com>\n\
                   Acked-by: User Name <email@example.invalid>\n\
                   Merge-request: !15\n";
    utils::simple_merge_behavior(service, MR_BY_EXPLICIT_ID, message)
}

#[test]
fn test_merge_trailer_other_by_trailer() {
    let service = merge_service();
    let message = "Merge topic 'topic-16' into test-merge\n\
                   \n\
                   7189cf5 topic-1: make a change\n\
                   \n\
                   Acked-by: Ghostflow <ghostflow@example.com>\n\
                   Reported-by: user <user@example.invalid>\n\
                   Merge-request: !16\n";
    utils::simple_merge_behavior(service, MR_OTHER_BY_TRAILER_ID, message)
}

#[test]
fn test_merge_ignored_trailer() {
    let service = merge_service();
    let message = "Merge topic 'topic-17' into test-merge\n\
                   \n\
                   7189cf5 topic-1: make a change\n\
                   \n\
                   Acked-by: Ghostflow <ghostflow@example.com>\n\
                   Merge-request: !17\n";
    utils::simple_merge_behavior(service, MR_IGNORED_TRAILER_ID, message)
}

#[test]
fn test_merge_awarded_100() {
    let service = merge_service();
    let message = "Merge topic 'topic-18' into test-merge\n\
                   \n\
                   7189cf5 topic-1: make a change\n\
                   \n\
                   Acked-by: Ghostflow <ghostflow@example.com>\n\
                   Acked-by: maint <maint@example.invalid>\n\
                   Merge-request: !18\n";
    utils::simple_merge_behavior(service, MR_AWARDED_100_ID, message)
}

#[test]
fn test_merge_awarded_clap() {
    let service = merge_service();
    let message = "Merge topic 'topic-19' into test-merge\n\
                   \n\
                   7189cf5 topic-1: make a change\n\
                   \n\
                   Acked-by: Ghostflow <ghostflow@example.com>\n\
                   Acked-by: maint <maint@example.invalid>\n\
                   Merge-request: !19\n";
    utils::simple_merge_behavior(service, MR_AWARDED_CLAP_ID, message)
}

#[test]
fn test_merge_awarded_tada() {
    let service = merge_service();
    let message = "Merge topic 'topic-20' into test-merge\n\
                   \n\
                   7189cf5 topic-1: make a change\n\
                   \n\
                   Acked-by: Ghostflow <ghostflow@example.com>\n\
                   Acked-by: maint <maint@example.invalid>\n\
                   Merge-request: !20\n";
    utils::simple_merge_behavior(service, MR_AWARDED_TADA_ID, message)
}

#[test]
fn test_merge_awarded_thumbsup() {
    let service = merge_service();
    let message = "Merge topic 'topic-21' into test-merge\n\
                   \n\
                   7189cf5 topic-1: make a change\n\
                   \n\
                   Acked-by: Ghostflow <ghostflow@example.com>\n\
                   Acked-by: maint <maint@example.invalid>\n\
                   Merge-request: !21\n";
    utils::simple_merge_behavior(service, MR_AWARDED_THUMBSUP_ID, message)
}

#[test]
fn test_merge_awarded_no_good() {
    let service = merge_service();
    let message =
        "This merge request may not be merged into `test-merge` because:\n\n  - rejected by \
         @maint";
    utils::failed_merge_behavior(service, MR_AWARDED_NOGOOD_ID, message);
}

#[test]
fn test_merge_awarded_thumbsdown() {
    let service = merge_service();
    let message =
        "This merge request may not be merged into `test-merge` because:\n\n  - rejected by \
         @maint";
    utils::failed_merge_behavior(service, MR_AWARDED_THUMBSDOWN_ID, message);
}

#[test]
fn test_merge_awarded_thumbsdown_tone() {
    let service = merge_service();
    let message =
        "This merge request may not be merged into `test-merge` because:\n\n  - rejected by \
         @maint";
    utils::failed_merge_behavior(service, MR_AWARDED_THUMBSDOWN_TONE_ID, message);
}

#[test]
fn test_merge_awarded_ignored() {
    let service = merge_service();
    let message = "Merge topic 'topic-25' into test-merge\n\
                   \n\
                   7189cf5 topic-1: make a change\n\
                   \n\
                   Acked-by: Ghostflow <ghostflow@example.com>\n\
                   Merge-request: !25\n";
    utils::simple_merge_behavior(service, MR_AWARDED_IGNORED_ID, message)
}

#[test]
fn test_merge_duplicate_trailer() {
    let service = merge_service();
    let message = "Merge topic 'topic-26' into test-merge\n\
                   \n\
                   7189cf5 topic-1: make a change\n\
                   \n\
                   Acked-by: Ghostflow <ghostflow@example.com>\n\
                   Acked-by: maint <maint@example.invalid>\n\
                   Merge-request: !26\n";
    utils::simple_merge_behavior(service, MR_DUPLICATE_TRAILER_ID, message)
}
