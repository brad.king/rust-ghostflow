// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::collections::hash_map::HashMap;
use std::sync::Arc;

use chrono::Utc;
use git_workarea::CommitId;

use crate::host::*;
use crate::tests::mock::MockService;

#[derive(Clone)]
pub struct MockIssue {
    issue: Issue,
    reject_labels: bool,
}

impl MockIssue {
    pub(in crate::tests::mock) fn issue(&self) -> &Issue {
        &self.issue
    }

    pub(in crate::tests::mock) fn should_reject_labels(&self) -> bool {
        self.reject_labels
    }
}

#[derive(Clone)]
pub struct MockMergeRequest {
    mr: MergeRequest,
    closes_issues: Vec<u64>,
    comments: Vec<(Comment, Vec<Award>)>,
    awards: Vec<Award>,
}

impl MockMergeRequest {
    pub fn new<'a, F>(id: u64, author: &User, commit: &str, to: &Repo, from: F) -> Self
    where
        F: Into<Option<&'a Repo>>,
    {
        Self::new_impl(id, author, commit, to, from.into())
    }

    fn new_impl(id: u64, author: &User, commit: &str, to: &Repo, from: Option<&Repo>) -> Self {
        let topic_name = format!("topic-{}", id);
        let mr = MergeRequest {
            source_repo: from.cloned(),
            source_branch: topic_name.clone(),
            target_repo: to.clone(),
            target_branch: "master".into(),
            id,
            url: format!("mr{}", id),
            work_in_progress: false,
            description: String::new(),
            old_commit: None,
            commit: Commit {
                repo: from.unwrap_or(to).clone(),
                id: CommitId::new(commit),
                refname: Some(topic_name),
                last_pipeline: None,
            },
            author: author.clone(),

            reference: format!("!{}", id),
            remove_source_branch: false,
        };

        Self {
            mr,
            closes_issues: Vec::new(),
            comments: Vec::new(),
            awards: Vec::new(),
        }
    }

    pub fn description<D>(mut self, description: D) -> Self
    where
        D: Into<String>,
    {
        self.mr.description = description.into();
        self
    }

    pub fn old_commit(mut self, old_commit: &str) -> Self {
        let src_repo = self.mr.source_repo.as_ref().unwrap_or(&self.mr.target_repo);
        self.mr.old_commit = Some(Commit {
            repo: src_repo.clone(),
            id: CommitId::new(old_commit),
            refname: None,
            last_pipeline: None,
        });
        self
    }

    pub fn with_pipeline(mut self, pipeline: u64) -> Self {
        self.mr.commit.last_pipeline = Some(pipeline);
        self
    }

    pub fn work_in_progress(mut self) -> Self {
        self.mr.work_in_progress = true;
        self
    }

    pub fn add_comment(mut self, comment: Comment) -> Self {
        self.comments.push((comment, Vec::new()));
        self
    }

    pub fn add_award(mut self, award: Award) -> Self {
        self.awards.push(award);
        self
    }

    pub(in crate::tests::mock) fn mr(&self) -> &MergeRequest {
        &self.mr
    }

    pub(in crate::tests::mock) fn comments(&self) -> &Vec<(Comment, Vec<Award>)> {
        &self.comments
    }

    pub(in crate::tests::mock) fn awards(&self) -> &Vec<Award> {
        &self.awards
    }

    pub(in crate::tests::mock) fn closes_issues(&self) -> &Vec<u64> {
        &self.closes_issues
    }
}

#[derive(Clone)]
pub struct MockPipeline {
    pipeline: Pipeline,

    jobs: Vec<u64>,
}

impl MockPipeline {
    pub fn new(id: u64, repo: &Repo, commit: &str) -> Self {
        Self {
            pipeline: Pipeline {
                state: PipelineState::Manual,
                commit: Commit {
                    repo: repo.clone(),
                    id: CommitId::new(commit),
                    refname: None,
                    last_pipeline: None,
                },
                id,
            },
            jobs: Vec::new(),
        }
    }

    pub fn add_job(&mut self, job: u64) -> &mut Self {
        self.jobs.push(job);
        self
    }

    pub(in crate::tests::mock) fn pipeline(&self) -> &Pipeline {
        &self.pipeline
    }

    pub(in crate::tests::mock) fn jobs(&self) -> &Vec<u64> {
        &self.jobs
    }
}

#[derive(Clone)]
pub struct MockData {
    projects: HashMap<String, Repo>,
    users: HashMap<String, User>,
    issues: HashMap<u64, MockIssue>,
    merge_requests: HashMap<u64, MockMergeRequest>,
    pipelines: HashMap<u64, MockPipeline>,
    jobs: HashMap<u64, PipelineJob>,
}

impl MockData {
    fn new() -> Self {
        MockData {
            projects: HashMap::new(),
            users: HashMap::new(),
            issues: HashMap::new(),
            merge_requests: HashMap::new(),
            pipelines: HashMap::new(),
            jobs: HashMap::new(),
        }
    }

    pub(in crate::tests::mock) fn find_user(&self, user: &str) -> Option<&User> {
        self.users.get(user)
    }

    pub(in crate::tests::mock) fn find_project(&self, project: &str) -> Option<&Repo> {
        self.projects.get(project)
    }

    pub(in crate::tests::mock) fn find_pipeline(&self, pipeline: u64) -> Option<&MockPipeline> {
        self.pipelines.get(&pipeline)
    }

    pub(in crate::tests::mock) fn pipelines(&self) -> impl Iterator<Item = &MockPipeline> {
        self.pipelines.values()
    }

    pub(in crate::tests::mock) fn find_job(&self, job: u64) -> Option<&PipelineJob> {
        self.jobs.get(&job)
    }

    pub(in crate::tests::mock) fn find_issue(&self, id: u64) -> Option<&MockIssue> {
        self.issues.get(&id)
    }

    pub(in crate::tests::mock) fn find_merge_request(&self, id: u64) -> Option<&MockMergeRequest> {
        self.merge_requests.get(&id)
    }

    pub fn builder() -> MockDataBuilder {
        MockDataBuilder {
            data: Self::new(),
        }
    }

    pub fn self_repo() -> Repo {
        Repo {
            name: "self".into(),
            url: concat!(env!("CARGO_MANIFEST_DIR"), "/../.git").into(),
            forked_from: None,
        }
    }

    pub fn repo(name: &str) -> Repo {
        Repo {
            name: name.into(),
            url: name.into(),
            forked_from: None,
        }
    }

    pub fn forked_repo(name: &str, upstream: &Repo) -> Repo {
        Repo {
            name: name.into(),
            url: name.into(),
            forked_from: Some(Box::new(upstream.clone())),
        }
    }

    pub fn user(name: &str) -> User {
        User {
            handle: name.into(),
            name: name.into(),
            email: format!("{}@example.invalid", name),
        }
    }

    pub fn job(repo: &Repo, state: PipelineState, stage: &str, id: u64) -> PipelineJob {
        PipelineJob {
            repo: repo.clone(),
            state,
            stage: Some(stage.into()),
            name: stage.into(),
            id,
        }
    }

    pub fn comment(author: &User, content: &str) -> Comment {
        Comment {
            id: format!("{}-{}", author.name, content),
            is_system: false,
            is_branch_update: false,
            created_at: Utc::now(),
            author: author.clone(),
            content: content.into(),
        }
    }

    pub fn award(author: &User, name: &str) -> Award {
        Award {
            name: name.into(),
            author: author.clone(),
        }
    }
}

pub struct MockDataBuilder {
    data: MockData,
}

impl MockDataBuilder {
    pub(in crate::tests::mock) fn from_data(data: MockData) -> Self {
        Self {
            data,
        }
    }

    pub(in crate::tests::mock) fn into_inner(self) -> MockData {
        self.data
    }

    pub fn service(&self) -> Arc<MockService> {
        let mut service_data = self.data.clone();

        let user = MockData::user("ghostflow");
        service_data.users.insert(user.name.clone(), user.clone());

        Arc::new(MockService::new(service_data, user))
    }

    pub fn add_project(&mut self, project: Repo) -> &mut Self {
        self.data.projects.insert(project.name.clone(), project);

        self
    }

    pub fn add_user(&mut self, user: User) -> &mut Self {
        self.data.users.insert(user.name.clone(), user);

        self
    }

    pub fn add_merge_request(&mut self, mr: MockMergeRequest) -> &mut Self {
        self.data.merge_requests.insert(mr.mr.id, mr);

        self
    }

    pub fn add_pipeline(&mut self, pipeline: MockPipeline) -> &mut Self {
        self.data.pipelines.insert(pipeline.pipeline.id, pipeline);

        self
    }

    pub fn add_job(&mut self, job: PipelineJob) -> &mut Self {
        self.data.jobs.insert(job.id, job);

        self
    }
}
