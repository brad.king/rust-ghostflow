// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::env;

use git_workarea::GitContext;
use tempfile::TempDir;

use crate::tests::log;

pub fn test_workspace_dir() -> TempDir {
    log::setup_logging();

    let mut working_dir = env::current_exe().unwrap();
    working_dir.pop();

    TempDir::new_in(working_dir).unwrap()
}

pub fn check_git_config_value(ctx: &GitContext, name: &str, expected: &str) {
    let value = get_git_config(ctx, name);
    let actual = value.trim();

    assert_eq!(actual, expected);
}

pub fn check_git_config_values(ctx: &GitContext, name: &str, expected: &[&str]) {
    let value = get_git_config(ctx, name);
    let actual = value.lines().collect::<Vec<_>>();

    assert_eq!(actual.len(), expected.len());
    assert_eq!(actual, expected);
}

fn get_git_config(ctx: &GitContext, name: &str) -> String {
    let config = ctx
        .git()
        .arg("config")
        .arg("--get-all")
        .arg(name)
        .output()
        .unwrap();

    String::from_utf8_lossy(&config.stdout).into_owned()
}
