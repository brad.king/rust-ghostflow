# ghostflow

This crate implements actions which are part of ghostflow, the git-hosted
development workflow. This includes things such as applying hook checks to a
commit, merging branches, handling testing commands, and so on. See the
documentation for the various actions for more information.
