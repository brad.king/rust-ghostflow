// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::sync::Arc;

use clap::{App, ArgMatches, SubCommand};
use thiserror::Error;

use crate::exit_code::ExitCode;
use crate::host::LocalService;

mod config;
pub use self::config::{Config, ConfigError, Read};

mod list;
use self::list::{List, ListError};

mod run;
use self::run::{Run, RunError};

#[derive(Debug, Error)]
#[non_exhaustive]
pub enum CheckError {
    #[error("the `check` subcommand requires a subcommand")]
    MissingCommand,
    #[error("unknown `check` subcommand `{}`", subcommand)]
    UnknownCommand { subcommand: String },
    #[error("{}", source)]
    List {
        #[from]
        source: ListError,
    },
    #[error("{}", source)]
    Run {
        #[from]
        source: RunError,
    },
}

impl CheckError {
    fn unknown_command(subcommand: String) -> Self {
        CheckError::UnknownCommand {
            subcommand,
        }
    }
}

type CheckResult<T> = Result<T, CheckError>;

pub struct Check;

impl Check {
    pub fn run(service: Arc<dyn LocalService>, matches: &ArgMatches) -> CheckResult<ExitCode> {
        match matches.subcommand() {
            ("list", Some(m)) => Ok(List::run(m)?),
            ("run", Some(m)) => Ok(Run::run(service, m)?),
            ("", _) => Err(CheckError::MissingCommand),
            (subcmd, _) => Err(CheckError::unknown_command(subcmd.into())),
        }
    }

    pub fn subcommand() -> App<'static, 'static> {
        SubCommand::with_name("check")
            .about("manage checks on a repository")
            .subcommand(List::subcommand())
            .subcommand(Run::subcommand())
    }
}
