// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::sync::Arc;

use clap::{App, Arg, ArgMatches, SubCommand};
use thiserror::Error;

use crate::checks::formatter::{Formatter, FormatterError};
use crate::exit_code::ExitCode;
use crate::host::LocalService;

mod commits;
use self::commits::{Commits, CommitsError};

mod topic;
use self::topic::{Topic, TopicError};

pub struct Run;

#[derive(Debug, Error)]
#[non_exhaustive]
pub enum RunError {
    #[error("the `check run` subcommand requires a subcommand")]
    NoSubcommand,
    #[error("unknown `check run` subcommand `{}`", subcommand)]
    UnknownCommand { subcommand: String },
    #[error("formatter error: {}", source)]
    Formatter {
        #[from]
        source: FormatterError,
    },
    #[error("{}", source)]
    Commits {
        #[from]
        source: CommitsError,
    },
    #[error("{}", source)]
    Topic {
        #[from]
        source: TopicError,
    },
}

impl RunError {
    fn unknown_command(subcommand: String) -> Self {
        RunError::UnknownCommand {
            subcommand,
        }
    }
}

type RunResult<T> = Result<T, RunError>;

impl Run {
    pub fn run(service: Arc<dyn LocalService>, matches: &ArgMatches) -> RunResult<ExitCode> {
        matches
            .values_of("FORMATTER")
            .map(Formatter::parse_args)
            .transpose()?;

        let config = matches.value_of("CONFIG");

        match matches.subcommand() {
            ("commits", Some(m)) => Ok(Commits::run(service, m, config)?),
            ("topic", Some(m)) => Ok(Topic::run(service, m, config)?),
            ("", _) => Err(RunError::NoSubcommand),
            (subcmd, _) => Err(RunError::unknown_command(subcmd.into())),
        }
    }

    pub fn subcommand() -> App<'static, 'static> {
        SubCommand::with_name("run")
            .about("run checks")
            .arg(
                Arg::with_name("CONFIG")
                    .short("c")
                    .long("config")
                    .help("Configuration for checks")
                    .takes_value(true),
            )
            .arg(
                Arg::with_name("FORMATTER")
                    .short("F")
                    .long("formatter")
                    .help("specify the path to a formatter `KIND=PATH`")
                    .takes_value(true)
                    .number_of_values(1)
                    .multiple(true),
            )
            .subcommand(Commits::subcommand())
            .subcommand(Topic::subcommand())
    }
}
